[[ $- != *i* ]] && return

export PATH=$PATH:~/Scripts

### PROMPT ### 

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

PS1='\u@\h \[\e[1;31m\]\w\e[m$(parse_git_branch) \$ '

### ALIASES ###

alias dirs='dirs -v'
alias lsa='ls -a'
alias up='. up'


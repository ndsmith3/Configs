"TAB

set tabstop=4 "number of spaces in tabs
set softtabstop=4 "number of spaces in tabs when editing
set expandtab
set autoindent 

"UI

syntax enable "enables syntax highlighting
set number "line numbers
set showcmd "shows last command
set wildmenu "autocomplete for search
set showmatch "highlights match for {[()]}

"SEARCHING 

set incsearch "shows matches as entered 
set hlsearch "highlights search matches

"FOLDING
set foldenable "enables folding
set foldlevelstart=5 "sets number of lines that fold

"REMAPS
inoremap jk <esc> "maps jk to escape 
